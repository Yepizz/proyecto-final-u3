import React,{Component} from 'react'
import { Text, View, TouchableOpacity, StyleSheet, Image } from 'react-native'

class SingleView extends Component{
     state = {
        data: []
    }

    componentDidMount = () => {
        fetch('https://yepiz.uthds4.info/api/productos', { method: 'GET' })
            .then((response) => response.json())
            .then((responseJson) => {
                console.log(responseJson);
                this.setState({
                    data: responseJson
                })
            }) 
            .catch((error) => {
                console.error(error);
            });
            
    }
    alertDescription = (item) => {
        alert(item.descripcion)
    }

    render() {
        return (
            
                <View style={styles.mainContainer}>
                {
                this.state.data.map((item, index) => (
                    <TouchableOpacity key={item.id} onPress={() => this.alertDescription(item)}>
                        <View style={styles.secondContainer}>
                            <Text style={styles.strongText}>{item.nombre}</Text>
                            <Text style={styles.textBrand}>{item.marca}</Text>
                            <Text style={styles.textBarcode}>{item.codigoBarras}</Text>
                            <View style={styles.thirdContainer}>
                            <Image style={styles.logo}
                            source={{
                                uri: 'https://www.superama.com.mx/Content/images/products/img_large/0750101111561L.jpg',
                            }}/>
                            </View>
                        </View>
                        </TouchableOpacity>
                    ))
                }
                </View>
                
        )
    }
} 

export default SingleView


const styles = StyleSheet.create({
    mainContainer: {
        backgroundColor: '#a02af5',
        width: 500,
        // height: 200,
        borderColor:'#000'
    },
    secondContainer: {
        width: 245,
        height: 100,
        marginLeft:25,
        marginTop: 5,
        marginRight: 16,
        marginBottom:5,
        borderColor: '#000',
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center',
        borderRadius:10,
        
    },
    thirdContainer: {
        backgroundColor: '#c287ed',
        borderColor:'#000',
        marginLeft: 1,
        marginBottom: 0,
        marginRight:115,
        width: 100,
        height: 100,
        borderRadius: 5,
        bottom: 30,
        right:10
    },
    strongText: {
        color: 'red',
        marginTop: 2,
        left: 20,
        top: 35,
        fontWeight:'bold'
        
    },
    textBrand: {
        top: 40,
        left: 20,
        color:'blue'
    },
    textBarcode: {
        top: 40,
        left: 35,
        color:'orange'
    },
    logo: {
        width: 100,
        height: 100,
        // borderRadius:15
    }
   

});